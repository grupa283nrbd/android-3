package com.example.timepicker;

        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.os.Build;

        import androidx.annotation.RequiresApi;
        import androidx.core.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder notificationBuilder = notificationHelper.getAlarmNotification();
        notificationHelper.getManager().notify(1, notificationBuilder.build());
    }
}